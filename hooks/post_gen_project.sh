#!/bin/bash

# Create and run on a virtual environment
virtualenv --python=`which python{{cookiecutter.use_python_version}}` venv
source venv/bin/activate

# Cleanup frontend files if django is used as default
{% if cookiecutter.use_frontend == 'Django' %}
rm -rf {{cookiecutter.project_slug}}/frontend
rm -f package.json
rm -f webpack.config.js
{% endif %}

# Cleanup files if API not required
{% if cookiecutter.use_drf != 'y' %}
rm -f {{cookiecutter.project_slug}}/{{cookiecutter.main_app}}/api_urls.py
rm -f {{cookiecutter.project_slug}}/{{cookiecutter.main_app}}/serializers.py
{% endif %}

# Cleanup files if Progressive Web App not required
{% if cookiecutter.use_pwa != 'y' %}
rm -rf {{cookiecutter.project_slug}}/frontend/static/pwa
{% endif %}

# Rename environment file
mv .env.example .env

# Add in files what we have for now
git init
git add .
# Run pre-commit twice (First time to fix code format, second time to ensure all good!)
pre-commit install
pre-commit run --all-files || pre-commit run --all-files
git add .

# Create standard branches and link to remote
{% if cookiecutter.upload_repo == "y" %}
git remote add origin git@bitbucket.org:wspdigitaluk/{{ cookiecutter.repo_name }}.git
git commit -m 'Initial setup commit'
git push -f --set-upstream origin master
git checkout -b test
git push -f --set-upstream origin test
git checkout -b stag
git push -f --set-upstream origin stag
git checkout master
{% endif %}

# Run migration, frontend setup if applicable and you good to go!
{% if cookiecutter.launch_project == 'y' %}
./scripts/launch_local_server.sh
{% if cookiecutter.use_frontend != 'Django' %}
./scripts/update_frontend.sh rebuild
{% endif %}
{% endif %}
