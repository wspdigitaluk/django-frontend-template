#!/bin/bash

REPO_NAME_FORMAT=^[a-z][a-z0-9\-]+$

repo_name="{{ cookiecutter.repo_name }}"

if ! [[ $repo_name =~ $REPO_NAME_FORMAT ]]; then
    echo "ERROR: $repo_name is not a valid repo name! It should start with an alphabet and only contains either alphabet, number or hyphen character only."

    exit 1
fi