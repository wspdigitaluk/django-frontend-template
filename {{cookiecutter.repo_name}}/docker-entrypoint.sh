#!/bin/bash

cd /data

echo 'Running migration...'
python3 /data/manage.py migrate
python3 /data/manage.py collectstatic --noinput

echo 'Running gunicorn...'
gunicorn config.wsgi:application
