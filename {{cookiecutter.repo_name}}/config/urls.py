"""config URL Configuration
"""
from django.contrib import admin
from django.conf.urls import include, url

admin.site.site_header = '{{cookiecutter.project_name}} App Administration'
admin.site.site_title = '{{cookiecutter.project_name}} App Administration'

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    {%- if cookiecutter.use_drf == 'y' %}
    url(r'^api/v1/{{cookiecutter.main_app}}/', include('{{cookiecutter.project_slug}}.{{cookiecutter.main_app}}.urls')),
    {%- endif %}
    {%- if cookiecutter.use_frontend == 'Django' %}
    url(r'', include('{{cookiecutter.project_slug}}.{{cookiecutter.main_app}}.urls')),
    {% else %}
    url(r'', include('{{cookiecutter.project_slug}}.frontend.urls')),
    {%- endif %}
    {%- if cookiecutter.use_pwa == 'y' %}
    url(r'', include('pwa.urls')),
    {%- endif %}
]
