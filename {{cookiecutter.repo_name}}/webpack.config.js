var path = require("path");

module.exports = {
  context: __dirname,
  devtool: "source-map",
  entry: "./{{cookiecutter.project_slug}}/frontend/src/index.js",
  output: {
    filename: "main.js",
    path: path.resolve(__dirname, "{{cookiecutter.project_slug}}/frontend/static/frontend/")
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
        }
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: ["file-loader?name=img/[name].[ext]"]
      }
    ]
  }
};
