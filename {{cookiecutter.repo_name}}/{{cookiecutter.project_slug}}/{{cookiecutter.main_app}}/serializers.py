from rest_framework.serializers import ModelSerializer

from .models import {{cookiecutter.main_model}}


class {{cookiecutter.main_model}}Serializer(ModelSerializer):
    class Meta:
        model = {{cookiecutter.main_model}}
        fields = ('name',)
