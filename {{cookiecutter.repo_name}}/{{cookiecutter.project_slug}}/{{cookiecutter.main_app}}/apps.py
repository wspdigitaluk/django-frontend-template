from django.apps import AppConfig


class {{cookiecutter.main_app}}Config(AppConfig):
    name = '{{cookiecutter.main_app}}'
