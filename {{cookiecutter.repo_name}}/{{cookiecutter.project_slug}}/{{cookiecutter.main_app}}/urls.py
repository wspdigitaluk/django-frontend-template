from django.conf.urls import url

from .views import {{cookiecutter.main_model}}View {% if cookiecutter.use_frontend == 'Django' %}, IndexView{% endif %}

urlpatterns = [
    url(r'^{{cookiecutter.main_model|lower}}/$', {{cookiecutter.main_model}}View.as_view(), name='{{cookiecutter.main_model}}_api_list'),
    {%- if cookiecutter.use_frontend == 'Django' %}
    url(r'^$', IndexView.as_view(), name='{{cookiecutter.main_model}}_list'),
    {%- endif %}
]