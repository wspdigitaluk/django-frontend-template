from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from django.conf import settings

def create_accounts(email, password, superuser):
    if superuser:
        if not email and not password:    
            email = settings.SUPERUSER_EMAIL
            password = settings.SUPERUSER_PASSWORD
            username = settings.SUPERUSER_USERNAME
        else:
            userlist = email.split('@')
            username = userlist[0] 

        if not User.objects.filter(username=username).exists():
            User.objects.create_superuser(username, email, password)
            msg = 'Superuser created!'
        else:
            msg = f'Superuser with {username} username already exists'
    else:
        userlist = email.split('@')
        username = userlist[0] 
        User.objects.create_user(username, email, password)
        msg = 'Normal user created!'

    return msg

class Command(BaseCommand):
    help = 'Manage users'

    def add_arguments(self, parser):
        parser.add_argument(
            '--email',
            action='store',
            help='Specify email for the account'
        )
        parser.add_argument(
            '--password',
            action='store',
            help='Specify password for the account'
        )
        parser.add_argument(
            '--superuser',
            action='store_true',
            help='Specify if superuser account is required'
        )

    def handle(self, *args, **options):
        email = options['email']
        password = options['password']
        superuser = options['superuser']
        if email and password or superuser:
            msg = create_accounts(email, password, superuser)
            self.stdout.write(msg)
        else:
            self.stderr.write('No argument provided. No user created')
