{% if cookiecutter.use_frontend == 'Django' %}from django.views import generic{% endif %}
from .models import {{cookiecutter.main_model}}

{%- if cookiecutter.use_drf == 'y' %}
from rest_framework import generics
from .serializers import {{cookiecutter.main_model}}Serializer

class {{cookiecutter.main_model}}View(generics.ListAPIView):
    queryset = {{cookiecutter.main_model}}.objects.all()
    serializer_class = {{cookiecutter.main_model}}Serializer
{%- endif %}

{%- if cookiecutter.use_frontend == 'Django' %}
class IndexView(generic.ListView):
    template_name = '{{cookiecutter.main_app}}/index.html'
    context_object_name = 'latest_{{cookiecutter.main_model|lower}}_list'

    def get_queryset(self):
        return {{cookiecutter.main_model}}.objects.order_by('name')
    
{%- endif %}