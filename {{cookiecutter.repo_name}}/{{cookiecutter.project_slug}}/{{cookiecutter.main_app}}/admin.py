from django.contrib import admin

from {{cookiecutter.project_slug}}.{{cookiecutter.main_app}}.models import {{cookiecutter.main_model}}

@admin.register({{cookiecutter.main_model}})
class {{cookiecutter.main_model}}Admin(admin.ModelAdmin):
    list_display = ('name',)
