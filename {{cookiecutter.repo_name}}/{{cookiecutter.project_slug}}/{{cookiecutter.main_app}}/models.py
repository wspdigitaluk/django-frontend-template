from django.db import models
from django.utils.translation import ugettext_lazy as _

class {{cookiecutter.main_model}}(models.Model):
    name = models.CharField(
        max_length=200,
        help_text=_('Provide a short, descriptive name')
    )

    def __str__(self):
        return self.name

