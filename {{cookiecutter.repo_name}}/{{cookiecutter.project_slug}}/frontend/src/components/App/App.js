import React, { Component, Fragment } from "react";

import "./App.css"

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      loaded: false,
      placeholder: "Loading"
    }
  }

  componentDidMount() {
    fetch("api/v1/{{ cookiecutter.main_app }}/{{ cookiecutter.main_model|lower }}")
      .then(response => {
        if (response.status > 400) {
          return this.setState(() => {
            return { placeholder: "Something went wrong!" }
          })
        }
        return response.json()
      })
      .then(data => {
        this.setState(() => {
          return {
            data,
            loaded: true
          }
        })
      })
  }

  render() {
    return (
      <Fragment>
        <div>Django with React front here!</div>
        <ul>
          {this.state.data.map((el, idx) => {
            return (
              <li key={idx}>
                {el.name}
              </li>
            )
          })}
        </ul>
      </Fragment>
    )
  }
}

export default App;
