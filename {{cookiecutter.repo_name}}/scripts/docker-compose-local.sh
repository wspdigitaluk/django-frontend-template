#!/bin/bash
CMD_PATH=$(dirname "$(realpath $0)")
cd $CMD_PATH/..
export COMPOSE_FILE=docker-compose.yml:docker-compose-local.yml

docker-compose "$@"
