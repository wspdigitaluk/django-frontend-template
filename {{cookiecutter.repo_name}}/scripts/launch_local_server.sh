#!/bin/bash
CMD_PATH=$(dirname "$(realpath $0)")

cd $CMD_PATH
DOCKER_COMPOSE=$CMD_PATH/docker-compose-local.sh
MANAGE_PY="${DOCKER_COMPOSE} exec app python3 manage.py"

if [ "$1" == "clean" ]; then
    echo "Bringing down all containers..."
    $DOCKER_COMPOSE down -v --remove-orphans
elif [ ! -z "$1" ]; then
    echo "Unrecognized parameter!"
fi 

echo "Launching application..."
$DOCKER_COMPOSE up -d

echo "Copying static files..."
$MANAGE_PY collectstatic --noinput

echo "Running database migrations..."
$MANAGE_PY migrate

echo "Creating default superuser..."
$MANAGE_PY create_user --superuser
