#!/bin/bash

docker-compose exec -T application python3 /data/manage.py collectstatic --no-input

docker-compose exec -T application python3 /data/manage.py migrate
