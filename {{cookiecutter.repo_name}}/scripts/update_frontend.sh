#!/bin/bash
CMD_PATH=$(dirname "$(realpath $0)")
PROJECT_PATH=$(dirname "$CMD_PATH")

if [ "$1" == "rebuild" ]; then
    if [ -d $PROJECT_PATH/node_modules ]; then
        echo "Removing current node_modules..."
        rm -rf $PROJECT_PATH/node_modules
    fi
    echo "No node_modules detected. Installing package dependencies..."
    npm install
elif [ ! -z "$1" ]; then
    echo "Unrecognized parameter!"
fi 

echo "Building webpack files..."
npm run build

echo "Collecting static files..."
docker-compose exec application python3 /data/manage.py collectstatic --no-input
