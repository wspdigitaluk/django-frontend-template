FROM quay.io/wspdigitalukteam/base-image-python3:python3
LABEL maintainer="Mobolaji Osinuga <mobolaji.osinuga@wsp.com>"
ENV DEBIAN_FRONTEND noninteractive
ENV PYTHONUNBUFFERED=1

RUN /usr/local/bin/python -m pip install --upgrade pip

RUN mkdir /data
COPY requirements.txt /data
COPY requirements /data/requirements

RUN pip3 install -r /data/requirements.txt
COPY {{cookiecutter.project_slug}} /data/{{cookiecutter.project_slug}}
COPY config /data/config
COPY manage.py /data/manage.py
COPY docker-entrypoint.sh /data/docker-entrypoint.sh
RUN ["chmod", "+x", "/data/docker-entrypoint.sh"]
