# {{cookiecutter.project_name}}

The repository for the {{cookiecutter.project_name}}, with main development in Django{%- if cookiecutter.use_frontend == 'React' %}and front end developed with React framework{%- endif %}.

## Getting Started

Clone the repo, make sure you can access it in an environment running docker.

## Using Vagrant
If you are not using the dev-utils vagrant box, skip to [Running the App](#Running The App).  Otherwise,
you will want to clone/move the repo to the shared directory on the machine.

SSH into the machine and continue with the instructions below.

Note #1: Make sure you have exposed relevant ports in Vagrantfile. For example, 7080 for Nginx and 5432 for Postgres
Note #2: Also check if your environment has Node and npm installed, if update to front end is required.

## Setup local dev environment

To launch the local dev server from scratch, run the following script

```
./scripts/launch_local_server.sh
```

This would trigger following actions:
- [Collectstatic](#Collectstatic)
- [Migrations](#Migrations)
- [Default user creation](#Default-user-creation)

Once the project is run, it can be viewed locally on browser as http://localhost.
For Django admin panel access, go to http://localhost/admin
{%- if cookiecutter.use_drf == 'y' %}
For API routes, go to localhost/api/v1/<urls>, which further urls are defined in ./{{cookiecutter.project_slug}}/{{cookiecutter.main_app}}/api_urls.py
{%- endif %}

## Manual Tasks

### Collectstatic

```
./scripts/docker-compose-local.sh exec app python3 manage.py collectstatic --no-input
```

Static files won't be served unless you connect via the nginx port (see above).

### Migrations

```
./scripts/docker-compose-local.sh exec app python3 manage.py migrate
```

### Default user creation

```
./scripts/docker-compose-local.sh exec app python3 create_user --superuser
```

This will create superadmin with default credentials as stated in settings.py file:

| Username | Email | Password |
|----------|----------------|--------------|
| admin | admin@a.com | !23Password | 

\*this may change depending on environment variable SUPERUSER_PASSWORD (see also config/settings.py)

{%- if cookiecutter.use_frontend == 'React' %}
### Updating front end

React frontend is one of the apps in this Django project. In order to get the changes reflected correctly, just run below script.
It basically rebuild the main.js files and collect the static files for Django reference
```
./scripts/update_frontend.sh
``` 
{%- endif %}

## Testing

This project use Pytest for most of its unit testing. Before you are able to run the test, relevant packages need to be installed first into the app. To do that

```
./scripts/docker-compose-local.sh exec app pip3 install -r requirements/test.txt
```

To run all available test

```
./scripts/docker-compose-local.sh exec app pytest {{ cookiecutter.project_slug }}
```

To run specific method

```
docker-compose exec -w /data application pytest {{cookiecutter.project_slug}}/<App Name>/tests/<Test File Name>.py::<Test Class Name>::<Test Method Name>
```
