
# Django Frontend Template

This is Django project template which will create a Rancher deployable with Postgres database.

## Dependencies

The dependenciess required are:

1. Python 3.8 and pip
2. cookiecutter (PyPI)
3. docker-compose and docker
4. NodeJS and npm

## Creating a Project

In order to use the project simply open a terminal in the location you'd like the service's repo to be stored and type:

```
#: cookiecutter git@bitbucket.org:wspdigitaluk/django-frontend-template.git      
```

There is no need to clone the base project manually unless developing the template. You will be prompted with the 
following options, sensible defaults will be generated based on the project name. For example if the project name is 
`Django React` you will get the following defaults:

```
repo_name [base-django]:
upload_repo [y]:
project_name [Base Django]:
project_slug [base_django]:
domain_name [base-django]:
main_app [core]:
main_model [BaseDjango]:
feeds_channel [#base_django_feeds]:
use_python_version [3]:
Select use_frontend:
1 - Django
2 - React
Choose from 1, 2 [1]:
use_drf [y]:
use_pwa [y]:
launch_project [y]:
```

More detail about the parameters are:

- repo_name: This is the name of the repo in wspdigital's Bitbucket. This will be given a default value based on the
   project name but you can change this if you like
- upload_repo: Enter 'y' to upload the repo to bitbucket. Any other selection will not push the created project to
   bitbucket for you. Selecting 'y' will __force__ push the new project to the remote and create `test`
   and `stag` branches based off the `master` branch
- project_name: This is the human readable name of the project. Stick to Title Case and spaces
- project_slug: This is the name of the project, of which it will generally be recognized programming wise
- domain_name: This name is used normally for Rancher config files
- main_app: This is the main Django app for the project, where API (if applicable) generated from
- main_model: Name of model for generated main app.
- feeds_channel: The slack channel for rancher webhooks (Make sure you create this beforehand)
- use_python_version: Specific python version to run on virtual environment. Python 3.8 is the recommended version to specify here; else run local python3 version by default
- use_frontend: Enter '1' (default) if you want to keep standard Django template. Enter '2' if you want
   to generate single page app using React library
- use_drf: Enter 'y' to make main app equipped with Django Rest Framework API. Anything else will not trigger anything
- use_pwa: Enter 'y' to set up the frontend with Progressive Web App settings. Anything else will not trigger anything
- launch_project: Enter 'y' to start the project. This assumes nothing running on port 80 (Nginx) and/or application port. If there is process running, this will possibly fail the project generation.

Your project will be ready once the script has been run. `cd` to designated repo to start developing

## Developing the Template

This template has a CI pipeline to ensure that the template generated is a functioning service. The pipeline runs the
file `test-creation-ci.sh` Which runs the cookiecutter template with the default values, apart from `upload_repo` which
is set to false by the `test-config.yaml` (Otherwise our CI pipeline will try and upload a repo to bitbucket). This script
generates a project and in the `post_gen_project.sh` it will do a cleanup based on selection.

When developing the template it is important to remember that you must modify the code in `{{cookiecutter.repo_name}}`
and __not__ in the output of the generation, as re-running cookiecutter will override any changes you have made, this
can make it difficult to test the project and development is easier if you follow a TDD approach and ensure the test
pipeline passes on the generated app.

Testing Rancher/Nginx is much harder and the workflow involves running the ./scripts/test-creation.sh on a repo that
is hooked up to rancher and seeing the resulted deployed project

# How do I use my template service?

This is covered in the README.md inside the generated repo


# Troubleshooting

First confirm you have all the dependencies and try again if not

## Script failed due to port in use

Its best to make sure you don't have any running containers or Databases. Free up the port by terminating any service
currently occupying it.
